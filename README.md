# Documentation for WikiPOC-a shared well
This is a national project at a very early stage.
For that reson, the rest of this documentation will be in swedish only.

Projektet utvecklar ett koncept för ett nytt IT-verktyg: ett stöd för gemensam insamling av, och en gemensam källa för, tidigare svårtillgänlig eller ej digitaliserad information om kompetensbehov. 

### Grupper och Projekt/Repositories

- Utöver denna grupp som innehåller övergripande [dokumentation](https://gitlab.com/groups/arbetsformedlingen/wikipoc-gemensam-k-lla/-/wikis/home) och sammanhang finns två repon med källkod i [Frontend](https://gitlab.com/arbetsformedlingen/wikipoc-gemensam-k-lla/frontend).

- Repot [wikipoc](https://gitlab.com/arbetsformedlingen/wikipoc-gemensam-k-lla/frontend/wikipoc) innehåller kod för att testa och undersöka förslag på gränssnitt med tilltänkta användare. I nuläget är utvecklingen helt webbläsarbaserad och inga inmatade data sparas.  

- Repot [wikipoc-infra](https://gitlab.com/arbetsformedlingen/wikipoc-gemensam-k-lla/frontend/wikipoc-infra) innehåller hantering av back-end under projektets olika faser.

### Mer information och frågor

Mer information om projektupplägg och avgränsningar hittar du på [gruppens wikisida](https://gitlab.com/groups/arbetsformedlingen/wikipoc-gemensam-k-lla/-/wikis/home?view=create). 

I den här fasen har vi begränsade möjligheter att möta nya önskemål, men frågor och synpunkter är alltid välkomna. Prova att besöka:

1. [Forum](https://forum.jobtechdev.se/c/vara-projekt/wikipoc/48)
2. [Community på DIGG för KLL-data](https://community.dataportal.se/category/35/kompetensf%C3%B6rs%C3%B6rjning-och-livsl%C3%A5ngtl%C3%A4rande-kll)


### Målgrupp

Nuvarande konceptutveckling syftar bland annat till att få en bättre bild av vilka målgrupper som har vilka intressen kopplat till verktyget. Mer om detta går att läsa i [projektets slutredovisning](https://forum.jobtechdev.se/uploads/short-url/7HJWuuUbduB8nxwrcLzyuD4TKPp.pdf).

Utgångspunkten är dock att verktyget kommer vara användbart i centrala delar av flertalet stödprocesser inom kompetensförsörjning och livslångt lärande.    


**Notera att denna sida inte är en supportkanal.**


## Av

Arbetsförmedlingen är avsändare för plattformen https://jobtechdev.se. Syftet med plattformen är att samarbeta baserat på öppna data och ny teknik, för en bättre arbetsmarknad.

